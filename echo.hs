import System.Environment 

main = do 
    args <- getArgs
    if null args
      then putStrLn ""
	  else putStrLn (unwords args)
